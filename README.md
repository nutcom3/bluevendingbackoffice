<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Step to run
1. run: 

       ./vendor/bin/sail up

2. migrate: 

       ./vendor/bin/sail artisan migrate  

3. seeder: 

       ./vendor/bin/sail artisan db:seed --class=InventorySeeder 

## Run development mode
      
     php artisan serve

## Run unit test

    ./vendor/bin/phpunit    

## Note

in root directory we attach `bluevending.postman_collection.json` (POSTMAN collection) for testing API

## How to add item to vending machine ?

call `api/vendingmachine/additem` see in postman collection

## How to add item to stock ?

call `api/inventory/additem` see in postman collection