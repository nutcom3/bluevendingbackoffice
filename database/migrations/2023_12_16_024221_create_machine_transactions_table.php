<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('machine_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vending_machine_id');
            $table->unsignedBigInteger('inventory_vending_machine_id');
            $table->integer('quantity');
            $table->enum('status', ['pending', 'completed', 'failed'])->default('completed');
            $table->decimal('total_price', 10, 2);
            $table->decimal('total_change', 10, 2)->default(0);
            $table->decimal('total_inserted_amount', 10, 2);

            $table->integer('coin_1')->default(0);
            $table->integer('coin_5')->default(0);
            $table->integer('coin_10')->default(0);
            $table->integer('banknote_20')->default(0);
            $table->integer('banknote_50')->default(0);
            $table->integer('banknote_100')->default(0);
            $table->integer('banknote_500')->default(0);
            $table->integer('banknote_1000')->default(0);

            $table->timestamps();
            // Foreign keys
            $table->foreign('vending_machine_id')->references('id')->on('vending_machine');
            $table->foreign('inventory_vending_machine_id')->references('id')->on('inventory_vending_machine');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('machine_transactions');
    }
};
