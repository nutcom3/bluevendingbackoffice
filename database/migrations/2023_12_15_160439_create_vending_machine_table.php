<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vending_machine', function (Blueprint $table) {
            $table->id(); // Auto-incrementing ID
            $table->string('machine_code')->unique();
            $table->string('location');
            $table->text('description')->nullable();
            $table->integer('coin_1')->default(0);
            $table->integer('coin_5')->default(0);
            $table->integer('coin_10')->default(0);
            $table->integer('banknote_20')->default(0);
            $table->integer('banknote_50')->default(0);
            $table->integer('banknote_100')->default(0);
            $table->integer('banknote_500')->default(0);
            $table->integer('banknote_1000')->default(0);
            $table->softDeletes();
            $table->decimal('total_balance', 10, 2)->default(0.00);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable(false);
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable(false);
        
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vending_machine');
    }
};
