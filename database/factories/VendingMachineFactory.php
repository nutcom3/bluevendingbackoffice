<?php


namespace Database\Factories;

use App\Models\VendingMachine;
use Illuminate\Database\Eloquent\Factories\Factory;

class VendingMachineFactory extends Factory
{
  protected $model = VendingMachine::class;

  public function definition(): array
  {
    return [
      'machine_code' => $this->faker->unique()->word,
      'location' => $this->faker->address,
      'description' => $this->faker->sentence,
      'coin_1' => $this->faker->numberBetween(0, 100),
      'coin_5' => $this->faker->numberBetween(0, 100),
      'coin_10' => $this->faker->numberBetween(0, 100),
      'banknote_20' => $this->faker->numberBetween(0, 100),
      'banknote_50' => $this->faker->numberBetween(0, 100),
      'banknote_100' => $this->faker->numberBetween(0, 100),
      'banknote_500' => $this->faker->numberBetween(0, 100),
      'banknote_1000' => $this->faker->numberBetween(0, 100),
    ];
  }
}
