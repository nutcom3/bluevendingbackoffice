<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $inventoryItems = [
            ['name' => 'Apple Juice', 'brand' => 'Juicy', 'description' => 'Fresh apple juice', 'category' => 'drink', 'quantity' => 20, 'value' => 50, 'reorder_level' => 10, 'image_url' => 'https://i.ibb.co/80RM5FK/applejuice.png'],
            ['name' => 'Chocolate Bar', 'brand' => 'ChocoDelight', 'description' => 'Milk chocolate bar', 'category' => 'food', 'quantity' => 50, 'value' => 8, 'reorder_level' => 20, 'image_url' => 'https://i.ibb.co/prHJ2DR/chocolatebar.png'],
            ['name' => 'Orange Juice', 'brand' => 'Juicy', 'description' => 'Fresh orange juice', 'category' => 'drink', 'quantity' => 30, 'value' => 5, 'reorder_level' => 15, 'image_url' => 'https://i.ibb.co/jZ0cBH5/orangejuice.png'],
            ['name' => 'Potato Chips', 'brand' => 'Crunchy', 'description' => 'Salted potato chips', 'category' => 'food', 'quantity' => 40, 'value' => 15, 'reorder_level' => 25, 'image_url' => 'https://i.ibb.co/fGLnYsb/potatochips.png'],
            ['name' => 'Lemonade', 'brand' => 'FreshSip', 'description' => 'Natural lemonade drink', 'category' => 'drink', 'quantity' => 25, 'value' => 20, 'reorder_level' => 12, 'image_url' => 'https://i.ibb.co/3yLw5qs/lemonade.png'],
            ['name' => 'Peanut Butter', 'brand' => 'Nutty', 'description' => 'Creamy peanut butter', 'category' => 'food', 'quantity' => 15, 'value' => 16, 'reorder_level' => 10, 'image_url' => 'https://i.ibb.co/59knVGJ/peanutbutter.png'],
            ['name' => 'Green Tea', 'brand' => 'ZenLeaf', 'description' => 'Organic green tea', 'category' => 'drink', 'quantity' => 30, 'value' => 25, 'reorder_level' => 18, 'image_url' => 'https://i.ibb.co/MMrF05y/greentea.png'],
            ['name' => 'Almond Cookies', 'brand' => 'CookieCrunch', 'description' => 'Delicious almond cookies', 'category' => 'food', 'quantity' => 120, 'value' => 120, 'reorder_level' => 20, 'image_url' => 'https://i.ibb.co/y4z3vdD/almondcookies.png'],
            ['name' => 'Grape Juice', 'brand' => 'Juicy', 'description' => 'Natural grape juice', 'category' => 'drink', 'quantity' => 22, 'value' => 89, 'reorder_level' => 12, 'image_url' => 'https://i.ibb.co/KG9RLwG/grapejuice.png'],
            ['name' => 'Granola Bars', 'brand' => 'EnergyBoost', 'description' => 'Mixed nuts granola bars', 'category' => 'food', 'quantity' => 14, 'value' => 1.80, 'reorder_level' => 15, 'image_url' => 'https://i.ibb.co/wKTzRbN/granolabars.png']
        ];

        foreach ($inventoryItems as $item) {
            DB::table('inventory')->insert([
                'name' => $item['name'],
                'brand' => $item['brand'],
                'description' => $item['description'],
                'category' => $item['category'],
                'quantity' => $item['quantity'],
                'value' => $item['value'],
                'image_url' => $item['image_url'],
                'reorder_level' => $item['reorder_level'],
            ]);
        }

        DB::table('vending_machine')->insert([
            'machine_code' => 'VM1001',
            'location' => 'Main Street, City Center',
            'description' => 'Vending machine located in a high foot traffic area.',
            'coin_1' => 100,
            'coin_5' => 200,
            'coin_10' => 150,
            'banknote_20' => 10,
            'banknote_50' => 5,
            'banknote_100' => 2,
            'banknote_500' => 1,
            'banknote_1000' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        for ($i = 1; $i <= 10; $i++) {
            DB::table('inventory_vending_machine')->insert([
                'vending_machine_id' => 1,
                'inventory_id' => $i,
                'item_amount' => 10,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
