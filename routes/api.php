<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\VendingMachineController;
use App\Http\Controllers\InventoryVendingMachineController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('inventory', InventoryController::class);
Route::resource('vendingmachine', VendingMachineController::class);

Route::post('checkout', [VendingMachineController::class, 'checkout']);
Route::post('calculate/money', [VendingMachineController::class, 'calculateMoney']);
Route::post('vendingmachine/additem', [InventoryVendingMachineController::class, 'addItem']);
Route::post('inventory/additem', [InventoryController::class, 'addItem']);

Route::get('/test', function () {
    return 'Hello World';
});