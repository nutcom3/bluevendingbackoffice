<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendingMachine extends Model
{
    use HasFactory;

    protected $table = 'vending_machine';

    protected $fillable = [
        'machine_code',
        'location',
        'description',
        'coin_1',
        'coin_5',
        'coin_10',
        'banknote_20',
        'banknote_50',
        'banknote_100',
        'banknote_500',
        'banknote_1000',
        'total_balance',
    ];

    public function inventory()
    {
        return $this->belongsToMany(Inventory::class, 'inventory_vending_machine');
    }

    public function machineTransactions()
    {
        return $this->hasMany(MachineTransactions::class);
    }
}
