<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InventoryVendingMachine extends Model
{
    use HasFactory;

    protected $table = 'inventory_vending_machine';

    protected $fillable = [
        'vending_machine_id',
        'inventory_id',
        'item_amount'
    ];

    public function vendingMachine()
    {
        return $this->belongsTo(VendingMachine::class, 'vending_machine_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }
}
