<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MachineTransactions extends Model
{
    use HasFactory;
    protected $table = 'machine_transactions';

    protected $fillable = [
        'vending_machine_id',
        'inventory_vending_machine_id',
        'inventory_id',
        'quantity',
        'status',
        'total_price',
        'total_change',
        'total_inserted_amount',
        'coin_1',
        'coin_5',
        'coin_10',
        'banknote_20',
        'banknote_50',
        'banknote_100',
        'banknote_500',
        'banknote_1000',
    ];

    public function inventoryVendingMachine()
    {
        return $this->belongsTo(InventoryVendingMachine::class, 'vending_machine_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

}
