<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $table = 'inventory';

    protected $fillable = [
        'name',
        'brand',
        'description',
        'category',
        'quantity',
        'value',
        'reorder_level',
    ];

    public function vendingMachines()
    {
        return $this->belongsToMany(VendingMachine::class, 'inventory_vending_machine');
    }
}
