<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function index()
    {
        return Inventory::all();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'product_name' => 'required|max:255',
            'quantity' => 'required|integer'
        ]);

        $inventory = Inventory::create($validatedData);
        return response()->json($inventory, 201);
    }

    public function show($id)
    {
        return Inventory::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $inventory = Inventory::findOrFail($id);

        $validatedData = $request->validate([
            'product_name' => 'required|max:255',
            'quantity' => 'required|integer'
        ]);

        $inventory->update($validatedData);
        return response()->json($inventory, 200);
    }

    public function destroy($id)
    {
        $inventory = Inventory::findOrFail($id);
        $inventory->delete();
        return response()->json(null, 204);
    }

    public function addItem(Request $request) {
        $data = $request->all();
        $inventory = Inventory::findOrFail($data['inventory_id']);

        $newAmount = $inventory['quantity'] + $data['amount'];
        $inventory->update([
            'quantity' => $newAmount
        ]);
        
        return response()->json([
            'status' => 'success',
            'inventory' => $inventory
        ]);
    }
}
