<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InventoryVendingMachine;
use App\Models\VendingMachine;
use App\Models\Inventory;

class InventoryVendingMachineController extends Controller
{
    public function addItem(Request $request)
    {
        try {
            $data = $request->all();
            $inventoryVendingMachine = InventoryVendingMachine::find($data['inventory_vending_machine_id']);
            if (!$inventoryVendingMachine) {
                return response()->json(['message' => 'Not found'], 404);
            }
            $newAmount = $data['amount'] + $inventoryVendingMachine['item_amount'];
            $inventoryVendingMachine->update([
                'item_amount' => $newAmount
            ]);

            $inventory = self::decreseInventory($data);

            return response()->json([
                'status' => 'success',
                'inventoryVendingMachine' => $inventoryVendingMachine,
                'inventory' => $inventory
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }

    public function decreseInventory($data)
    {
        $inventory = Inventory::findOrFail($data['inventory_id']);

        if ($inventory['quantity'] < $data['amount']) {
            throw new \Exception('I item in inventory not enough');
        }
        $newAmount = $inventory['quantity'] - $data['amount'];
        $inventory->update([
            'quantity' => $newAmount
        ]);
        return $inventory;
    }
}
