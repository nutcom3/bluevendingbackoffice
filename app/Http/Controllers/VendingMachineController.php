<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VendingMachine;
use App\Models\MachineTransactions;
use App\Models\InventoryVendingMachine;
use Illuminate\Support\Facades\DB;

use function Psy\debug;

class VendingMachineController extends Controller
{
    public function index()
    {
        return MachineTransactions::all();
    }

    public function show($id)
    {
        $results = DB::table('inventory_vending_machine')
            ->join('inventory', 'inventory_vending_machine.inventory_id', '=', 'inventory.id')
            ->where('inventory_vending_machine.vending_machine_id', $id)
            ->select('inventory_vending_machine.*', 'inventory.name', 'inventory.brand', 'inventory.description', 'inventory.value', 'inventory.image_url')
            ->get();

        return response()->json([
            'data' => $results,
        ]);
    }

    public function store(Request $request)
    {
        $vendingMachine = VendingMachine::create($request->all());
        return response()->json($vendingMachine, 201);
    }

    public function update(Request $request, $id)
    {
        $vendingMachine = VendingMachine::find($id);
        if (!$vendingMachine) {
            return response()->json(['error' => 'Vending Machine not found'], 404);
        }
        $vendingMachine->update($request->all());
        return response()->json($vendingMachine);
    }

    public function destroy($id)
    {
        $vendingMachine = VendingMachine::find($id);
        if (!$vendingMachine) {
            return response()->json(['error' => 'Vending Machine not found'], 404);
        }
        $vendingMachine->delete();
        return response()->json(null, 204);
    }

    public function itemList(Request $request)
    {
        $data = $request->all();

        $results = DB::table('inventory_vending_machine')
            ->where('vending_machine_id', $data['vending_machine_id'])
            ->get();

        return response()->json([
            'data' => $results,
        ]);
    }

    public function checkout(Request $request)
    {
        try {
            $payload = $request->all();
            $changeAmount = $payload['total_inserted_amount'] - $payload['total_price'];
            $vendingMachine = VendingMachine::find($payload['vending_machine_id']);
            if (!$vendingMachine) {
                throw new \Exception('Vending machine not found');
            }

            $inventoryVendingMachine = InventoryVendingMachine::find($payload['inventory_vending_machine_id']);
            if (!$vendingMachine) {
                throw new \Exception('Inventory id not found');
            }

            if ($inventoryVendingMachine['item_amount'] <= 0) {
                throw new \Exception('Selected Item Sold out');
            }

            /* How much change do I have to give? */
            $bankNoteChange = self::calculateChange($changeAmount, $vendingMachine->toArray());

            /* Decrement banknote in current machine */
            $newVendingMachineState = self::decrementBankNotes($bankNoteChange, $vendingMachine->toArray());

            /* Increment banknote in current machine */
            $newVendingMachineState = self::incrementBankNotes($payload, $newVendingMachineState);

            /* Update total balance */
            $newVendingMachineState['total_balance'] = $vendingMachine['total_balance'] + $payload['total_price'];

            /* Update Current state of banknote */
            self::updateVendingMachine($newVendingMachineState);

            /* Decrement Stock (Vending machine can be 1 item per times) */
            self::decrementStock($payload['inventory_vending_machine_id']);

            /* Create to machine transaction */
            self::createMachineTransaction($payload, $bankNoteChange);

            return response()->json([
                'status' => 'success',
                'inventory_vending_machine_id' => $payload['inventory_vending_machine_id'],
                'total_price' => $payload['total_price'],
                'total_inserted_amount' => $payload['total_inserted_amount'],
                'change_amount' => $changeAmount,
                'banknotes_change' => $bankNoteChange,
                'newVendingMachineState' => $newVendingMachineState,
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }

    /**
     * Create a new machine transaction.
     *
     * @param array $data The data for the transaction.
     * @param array $bankNoteChange The banknote change for the transaction.
     * @return void
     */
    public function createMachineTransaction($data, $bankNoteChange)
    {
        MachineTransactions::create([
            'inventory_vending_machine_id' => $data['inventory_vending_machine_id'],
            'vending_machine_id' => $data['vending_machine_id'],
            'quantity' => 1,
            'total_price' => $data['total_price'],
            'total_inserted_amount' => $data['total_inserted_amount'],
            'coin_1' => $bankNoteChange['coin_1'] ?? 0,
            'coin_5' => $bankNoteChange['coin_5'] ?? 0,
            'coin_10' => $bankNoteChange['coin_10'] ?? 0,
            'banknote_20' => $bankNoteChange['banknote_20'] ?? 0,
            'banknote_50' => $bankNoteChange['banknote_50'] ?? 0,
            'banknote_100' => $bankNoteChange['banknote_100'] ?? 0,
            'banknote_500' => $bankNoteChange['banknote_500'] ?? 0,
            'banknote_1000' => $bankNoteChange['banknote_1000'] ?? 0,
        ]);
    }

    /**
     * Decrement the stock of a vending machine inventory item.
     *
     * @param int $inventoryId The ID of the inventory item.
     * @return void
     */
    public function decrementStock($inventoryId)
    {
        DB::table('inventory_vending_machine')
            ->where('inventory_id', $inventoryId)
            ->decrement('item_amount', 1);
    }

    /**
     * Decrements the banknotes in the vending machine based on the given banknotes change.
     *
     * @param array $bankNoteChange The banknotes change to be applied to the vending machine.
     * @param array $vendingMachine The current banknotes of the vending machine.
     * @return array The updated state of the vending machine after decrementing the banknotes.
     */
    public function decrementBankNotes($bankNoteChange, $vendingMachine)
    {
        foreach ($bankNoteChange as $key => $value) {
            if (isset($vendingMachine[$key])) {
                $vendingMachine[$key] = max($vendingMachine[$key] - $value, 0);
            }
        }
        return $vendingMachine;
    }

    /**
     * Increments the bank notes in the vending machine.
     *
     * @param array $bankNoteChange The array containing the bank note changes.
     * @param array $vendingMachine The array representing the vending machine.
     * @return array The updated vending machine array.
     */
    public function incrementBankNotes($bankNoteChange, $vendingMachine)
    {
        foreach ($bankNoteChange as $key => $value) {
            if (isset($vendingMachine[$key])) {
                $vendingMachine[$key] = $vendingMachine[$key] + $value;
            }
        }
        return $vendingMachine;
    }

    /**
     * Update a vending machine.
     *
     * @param array $newVendingMachine The new vending machine data.
     * @return void
     */
    public function updateVendingMachine($newVendingMachine)
    {
        $vendingMachine = VendingMachine::find($newVendingMachine['id']);
        $vendingMachine->update($newVendingMachine);
    }

    function calculateMoney(Request $request)
    {
        $banknotes = config('app.banknotes');
        $total = 0;

        foreach ($request->all() as $type => $count) {
            if (isset($banknotes[$type])) {
                $total += $banknotes[$type] * $count;
            }
        }
        return response()->json([
            'status' => 'success',
            'total_inserted_amount' => $total,
        ]);
    }

    /**
     * Calculates the change to be given by a vending machine. Using Greedy Algorithm.
     *
     * @param float $changeAmount The amount of change to be given.
     * @param array $vendingMachine The vending machine's banknotes and their quantities.
     * @return array The calculated change as an associative array.
     * @throws \Exception If the change cannot be fully provided.
     */
    function calculateChange($changeAmount, $vendingMachine)
    {
        $banknotes = array_reverse([
            "coin_1" => 1,
            "coin_5" => 5,
            "coin_10" => 10,
            "banknote_20" => 20,
            "banknote_50" => 50,
            "banknote_100" => 100,
            "banknote_500" => 500,
            "banknote_1000" => 1000,
        ]);
        $change = [];
        $remainingAmount = $changeAmount;

        foreach ($banknotes as $banknoteType => $banknoteValue) {
            // if bank = 0
            if ($vendingMachine[$banknoteType] <= 0) {
                continue;
            }

            $count = intdiv($remainingAmount, $banknoteValue);
            if ($count > $vendingMachine[$banknoteType]) {
                $count = $vendingMachine[$banknoteType];
            }

            if ($count > 0) {
                $change[$banknoteType] = $count;
                $remainingAmount -= $count * $banknoteValue;
            }

            if ($remainingAmount == 0) {
                break;
            }
        }

        if ($remainingAmount > 0) {
            throw new \Exception('Change not enough');
        }
        return $change;
    }
}
