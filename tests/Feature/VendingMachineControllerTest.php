<?php

namespace Tests\Unit;

use App\Http\Controllers\VendingMachineController;
use PHPUnit\Framework\TestCase;

class VendingMachineControllerTest extends TestCase
{
    public function test_calculateChange_returns_change10(): void
    {
        $changeAmount = 10;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 98,
            "coin_5" => 198,
            "coin_10" => 150,
            "banknote_20" => 9,
            "banknote_50" => 5,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
        ];

        $expected = [
            'coin_10' => 1,
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change30(): void
    {
        $changeAmount = 30;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 98,
            "coin_5" => 198,
            "coin_10" => 150,
            "banknote_20" => 9,
            "banknote_50" => 5,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
        ];

        $expected = [
            'coin_10' => 1,
            'banknote_20' => 1,
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change50(): void
    {
        $changeAmount = 50;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 98,
            "coin_5" => 198,
            "coin_10" => 150,
            "banknote_20" => 9,
            "banknote_50" => 5,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
        ];

        $expected = [
            'banknote_50' => 1
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change500(): void
    {
        $changeAmount = 500;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 10,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 0,
        ];

        $expected = [
            'banknote_50' => 10
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change1000(): void
    {
        $changeAmount = 1000;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 98,
            "coin_5" => 198,
            "coin_10" => 150,
            "banknote_20" => 9,
            "banknote_50" => 5,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
        ];

        $expected = [
            'banknote_1000' => 1
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change1000_2(): void
    {
        $changeAmount = 1000;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 0,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
        ];

        $expected = [
            'banknote_1000' => 1
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change1100_error(): void
    {
        $changeAmount = 1100;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 0,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
            "total_balance" => "268.00",
        ];

        $this->expectExceptionMessage('Change not enough');

        $controller = new VendingMachineController();
        $controller->calculateChange($changeAmount, $vendingMachine);
    }

    public function test_calculateChange_returns_change500_error(): void
    {
        $changeAmount = 500;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 0,
            "banknote_100" => 0,
            "banknote_500" => 0,
            "banknote_1000" => 1,
            "total_balance" => "268.00",
        ];

        $this->expectExceptionMessage('Change not enough');

        $controller = new VendingMachineController();
        $controller->calculateChange($changeAmount, $vendingMachine);
    }

    public function test_calculateChange_returns_change10000(): void
    {
        $changeAmount = 10000;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 0,
            "banknote_100" => 0,
            "banknote_500" => 10,
            "banknote_1000" => 5,
        ];

        $expected = [
            'banknote_500' => 10,
            'banknote_1000' => 5
        ];

        $controller = new VendingMachineController();
        $actualChange = $controller->calculateChange($changeAmount, $vendingMachine);
        $this->assertEquals($expected, $actualChange);
    }

    public function test_calculateChange_returns_change10000_error(): void
    {
        $changeAmount = 10000;
        $vendingMachine = [
            "id" => 1,
            "coin_1" => 0,
            "coin_5" => 0,
            "coin_10" => 0,
            "banknote_20" => 0,
            "banknote_50" => 0,
            "banknote_100" => 0,
            "banknote_500" => 10,
            "banknote_1000" => 4,
        ];

        $this->expectExceptionMessage('Change not enough');

        $controller = new VendingMachineController();
        $controller->calculateChange($changeAmount, $vendingMachine);
    }
}

